(function () {
  
  var BASE_API_URL = 'http://driveby.gaspasa.com.mx:8090/AppGaspasa/';
  var API_KEY = 'ef73781effc5774100f87fe2f437a435';
  
  var app = ons.bootstrap('app', ['onsen']);
  
  function defaultView(profile, state) {
    if (profile === PROFILE_CI) {
      switch(state) {
          case ORDER_ASSIGNING: 
            return 'views/home/index_assiging.html';
            break;
          case ORDER_IN_ROUTE: 
            return 'views/home/index_in_route.html';
            break;
          case ORDER_ARRIVED: 
            return 'views/home/index_arrived.html';
            break;
          case ORDER_RATING: 
            return 'views/home/index_rating.html';
            break;
          default:
            return 'views/home/index.html';
            break;
      }
    }

    return 'views/home_te/index.html';
  }
  
  app.run(function($rootScope) {
    $rootScope.moment = moment;
    
    $rootScope.MINUTES_PROMISE = MINUTES_PROMISE;

    $rootScope.REQUEST_CHECK_INTERVAL_SHORT = REQUEST_CHECK_INTERVAL_SHORT;
    $rootScope.REQUEST_CHECK_INTERVAL_LONG = REQUEST_CHECK_INTERVAL_LONG;

    $rootScope.USER_PROFILE = USER_PROFILE;
    $rootScope.PROFILE_CI = PROFILE_CI;
    $rootScope.PROFILE_TE = PROFILE_TE;
    $rootScope.USER_DATA = USER_DATA;
    $rootScope.CUR_ORDER = CUR_ORDER;

    $rootScope.ORDER_ASSIGNING = ORDER_ASSIGNING;
    $rootScope.ORDER_IN_ROUTE = ORDER_IN_ROUTE;
    $rootScope.ORDER_ARRIVED = ORDER_ARRIVED;
    $rootScope.ORDER_RATING = ORDER_RATING;
    
    $rootScope.report = function () {
      Storage.getPlacePhone(function (phone){
        callPhone(phone);
      });
    }
  })
  
  app.directive('spinnerSelector', function($parse) {
    return {
      restrict: 'E',
      templateUrl: 'spinner-selector.html',
      scope: { 
        onChange: '=onChange',
        spinnerValue: '=spinnerValue',
        spinnerText: '=spinnerText',
        spinnerBase: '=spinnerBase'
      },
      link: function (scope, element, attributes, parentController) {
        
        var spinner = element[0].querySelector('.wrapper .spinner');
        var filler = element[0].querySelector('.wrapper .filler');
        var mask = element[0].querySelector('.wrapper .mask');
        var clock = element[0].querySelector('.wrapper .clock-layout');
        
        scope.spinnerBase = scope.spinnerBase || 30;
        moveSpinner(scope.spinnerValue * scope.spinnerBase);
        
        scope.$watch('spinnerValue', function (newValue, oldValue) {
          moveSpinner(+newValue * scope.spinnerBase);
        });

        function setDegrees(degrees) {
          degrees = degrees == 360 ? 0 : degrees;
          degrees += (degrees > 360) ? -(parseInt(degrees / 360) * 360) : 0;
          
          scope.$apply(function(){
            scope.spinnerValue = degrees / scope.spinnerBase;
            scope.onChange(degrees / scope.spinnerBase);
          });
        }
        
        function  moveSpinner(degrees) {
          degrees = degrees == 360 ? 0 : degrees;
          degrees += (degrees > 360) ? -(parseInt(degrees / 360) * 360) : 0;
          
          spinner.style.transform = 'rotate(' + degrees + 'deg)';

          if (degrees > 180) {
            filler.style.opacity = '1';
            mask.style.opacity = '0';
          } else {
            filler.style.opacity = '0';
            mask.style.opacity = '1';
          }
        }

        function round(deg, interval) {
          return Math.ceil(deg / interval) * interval;
        }

        function move(clock, e) {
          var dx = e.offsetX - (clock.offsetWidth / 2);               // horizontal offset from center
          var dy = e.offsetY - (clock.offsetHeight / 2);               // vertical offset from center

          var theta = Math.atan2(dy, dx);  // angle clockwise from X-axis, range -π .. π
          if (theta < 0) {                 // correct to range 0 .. 2π if desired
              theta += 2.0 * Math.PI;
          }

          var deg = (theta * 180 / Math.PI) + 90;
          setDegrees(round(deg, 30));
        }

        angular.element(clock).on('click', function (e) {
          move(this, e);
        });

        var sliderMouseDown = false;
        angular.element(clock).on('mousedown', function(e) {
          move(this, e);
          sliderMouseDown = true;
          return false;
        });
        
        angular.element(clock).on('mousemove', function(e) {
          if (sliderMouseDown) {
            move(this, e);
            return false;
          }
        });
        
        angular.element(clock).on('mouseup', function(e) {
          if (sliderMouseDown){
            sliderMouseDown = false;
            return false;
          }
        });
      }
    }
  });
  
  app.factory('HTTP', function ($http) {
    return function (url, data) {
      return $http({
        method: 'POST',
        url: BASE_API_URL + url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
        },
        data: data
      })
    }
  });
  
  app.factory('WS', function(HTTP) {
    return {
      getMenuOptions: function(data, callback) {
        var options = [
          {
            name: 'Tu pedido',
            icon: 'fa-truck',
            view: 'views/order/index.html',
            profiles: ['TE']
          },
          {
            name: 'Consumos',
            icon: 'fa-usd',
            view: 'views/consumption/index.html',
            profiles: ['CI']
          },
          {
            name: 'Consumos',
            icon: 'fa-usd',
            view: 'views/tanks/index.html',
            profiles: ['TE']
          },
          {
            name: 'Puntos',
            icon: 'fa-cubes',
            view: 'views/points/index.html',
            profiles: ['CI', 'TE']
          },
          {
            name: 'Pagos',
            icon: 'fa-credit-card',
            view: 'views/payments/index.html',
            profiles: ['TE']
          },
          {
            name: 'Facturación',
            icon: 'fa-file-text-o',
            view: 'views/billing/index.html',
            profiles: ['CI', 'TE']
          },
          {
            name: 'Promociones',
            icon: 'fa-tag',
            view: 'views/promotions/index.html',
            profiles: ['CI', 'TE']
          },
          {
            name: 'Ayuda',
            icon: 'fa-question',
            view: 'views/help/index.html',
            profiles: ['CI', 'TE']
          },
          {
            name: 'Configuraciones',
            icon: 'fa-cog',
            view: 'views/settings/index.html',
            profiles: ['CI']
          },
          {
            name: 'Configuraciones',
            icon: 'fa-cog',
            view: 'views/settings_te/index.html',
            profiles: ['TE']
          },
          {
            name: 'Acerca de',
            icon: 'fa-lightbulb-o',
            view: 'views/about/index.html',
            profiles: ['CI', 'TE']
          }
        ];
        
        callback(null, options.filter(function (option) {
          return option.profiles.includes(data.profile)
        }));
      },
      getPlaces: function (data, callback) {
        var places = [
          {id: 'MZ', name: 'Mazatlán'},
          {id: 'CL', name: 'Culiacan'},
          {id: 'LP', name: 'La Paz'},
          {id: 'CY', name: 'Celaya'},
          {id: 'OB', name: 'Cd. Obregon'},
          {id: 'NV', name: 'Navojoa'},
          {id: 'HM', name: 'Hermosillo'},
          {id: 'IR', name: 'Irapuato'},
          {id: 'LM', name: 'Los mochis'},
        ];
        
        HTTP('Plazas/tel_plazas', {
          key: API_KEY
        }).then(function (response) {
          callback(null, response.data['info_plazas'].map(function (place) {
            return {
              id: place['Plazas'].plaza,
              name: place['Plazas'].nombre,
              phone: place['Plazas'].tel_plaza
            }
          }));
        }).catch(function (error) {
          
        });
      },
      registerInfoClient: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('AltaCliente/alta_cliente', data).then(function (response) {
          delete data.key;
          callback(null, data);
        }).catch(function (error) {
          
        });
      },
      getRegisteredInfoClient: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Cliente/consulta_cliente', data).then(function (response) {
          delete data.key;
          
          if (response.data.tipo_servicio === 'P') {
            data.profile = [PROFILE_CI]; 
          } else {
            data.profile = [PROFILE_TE]; 
          }
          
          callback(null, data);
        }).catch(function (err) {
          
        })
      },
      getClientInfo: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Registro/registro', data).then(function (response) {
          if (response.data.Error && response.data.Error !== 0) {
            callback(null, null);
          }
          
          if (!response.data.nombre_cliente) {
            callback(null, null);
          }
          
          for(var key in response.data) {
            data[key] = response.data[key];
          }
          
          delete data.key;
          
          if (data.tipo_servicio === 'P') {
            data.profile = [PROFILE_CI]; 
          } else {
            data.profile = [PROFILE_TE]; 
          }

          callback(null, data);
        }).catch(function (error) {

        });
      },
      getAddresses: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Direcciones/consulta_direcciones', data).then(function (response) {
          var data = response.data;
          
          if (!!data['padron_id']) {
            delete data.Error;
            data = [ [data] ];
          }
          
          callback(null, data[0]);
        }).catch(function (error) {
          
        });
      },
      getCylindersInfo: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('ConsultaPrecio/consulta_precio', data).then(function (response) {          
          callback(null, response.data);
        }).catch(function (error) {
          
        });
      },
      orderCylinder: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('SolicitarCil/solicita_cil', data).then(function (response) {
          delete data.key;
          data.hora_registro = new Date(response.data['Hora pedido']);
          callback(null, data)
        }).catch(function (error) {
          
        });
      },
      assignUnitOrder: function (data, callback) {
        var order = angular.copy(data);
        order.key = API_KEY;
        
        HTTP('Asignacion/asignacion_unidad', order).then(function (response) {
          if (!!response.data.numero_unidad) {
            order.unit = response.data; 
          }
          
          callback(null, order);
        }).catch(function (error) {
          
        });
      },
      checkArriveOrder: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Arribo/arribo_unidad', data).then(function (response) {
          delete data.key;
          
          data.hora_llegada = new Date(response.data.hora_llegada);
          data.gana_promocion = response.data.gana_promocion;
          callback(null, data);
        }).catch(function (error) {
          
        });
      },
      rateOrder: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Califica/califica_unidad', data).then(function (response) {
          delete data.key;
          callback(null, data);
        }).catch(function (error) {
          
        });
      },
      getPromotions: function (data, callback) {
        var data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Promociones/promociones', data).then(function (response) {
          delete data.key;
          
          var promotions = response.data['Promocion:'];
          
          if (!Array.isArray(promotions)) {
            promotions = [ promotions ]
          }
          
          callback(null, promotions.map(function (promotion, index) {
            return Array.isArray(promotion) 
              ? { id: index, url: promotion[0] }
              : { id: index, url: promotion };
          }));
        }).catch(function (error) {
          
        })
      },
      saveGrlConfig: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Configral/config_gral', data).then(function(response) {
          delete data.key;
          callback(null, data);
        }).catch(function (error) {
          
        });
      },
      getOrderRateQuestions: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Preguntas/califica_preguntas', data).then(function (response) {
          callback(null, response.data);
        }).catch(function (error) {
          
        });
      },
      cancelOrder: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Cancela/cancela_pedido', data).then(function (response) {
          delete data.key;
          callback(null, data);
        }).catch(function (error) {
          
        });
      },
      getConsumptionInfo: function (data, callback) {
        data = angular.copy(data);
        data.key = API_KEY;
        
        HTTP('Consumo/consumo_cil', data).then(function (response) {
          callback(null, response.data['consumo'].map(function (c) {
            return c['Consumo'];
          }));
        }).catch(function (error) {
          
        });
      },
      getHelpInfo: function (data, callback) {
        data = angular.copy(data);
        HTTP('Ayuda/ayuda').then(function (response) {
          
        }).catch(function (error) {
          
        });
      }
    }
  })
  
  app.controller('LoadingController', function($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        Storage.getCurrentOrder(function (order) {
          Storage.setCurrentOrderState(order || {  });
        });
        
        Storage.getUserData(function (currentUser) {
          if (!!currentUser) {
            WS.getRegisteredInfoClient(currentUser, function (err, client) {
              var profile = ''
              
              if (client.profile.length === 1) {
                profile = client.profile[0];
                Storage.setProfile({ profile: profile })
              }
              
              startNavigator.resetToPage('main.html', { data: { client: client, profile: profile } });
            })
          } else {
            startNavigator.resetToPage('views/start/start.html');
          }
          
          Storage.setProfile({ profile: '' })
        });
      }
    }
  });
  
  app.controller('StartController', function($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.client = {}
        //self.client.celular_cliente = '6691115253';
        self.readOnlyPhone = false;
        
        Storage.getPhoneNumber(function (phone) {
          if (!!phone && phone.length > 0 && !isNaN(+phone)) {
            self.client.celular_cliente = phone;
            //self.client.celular_cliente = '6691115253';
            self.readOnlyPhone = true;
          }
        });
        
        Storage.getLastEmail(function (email) {
          if (!!email && email.length > 0) {
            self.client.correo_cliente = email;
          }
        });

        WS.getPlaces({}, function (err, places) {
          if (err) return;

          self.places = places;
          document.querySelector('#phonePlaceSpan').style.display = 'inline-block';
        })
      }
    }
    
    self.isFormComplete = function () {
      return !!self.acceptTerms && !!self.client.correo_cliente && !!self.client.celular_cliente && validateEmail(self.client.correo_cliente);
    }
    
    self.termCheckChanged = function () { 
      if (self.acceptTerms) {
        ons
        .createDialog('views/start/terms.html', { parentScope: $scope })
        .then(function(dialog) {
          dialog.show();
        });
      }
    }
    
    $scope.accept = function () {
      $scope.termsModal.hide();
    }
    
    $scope.decline = function () {
      self.acceptTerms = false;
      $scope.termsModal.hide();
    }
    
    self.imagePicker = function () {
      
    }
    
    self.getSelectedPlace = function () {
      if (!self.places) return;
      
      var client = angular.copy(self.client);
      if (!client.plaza) {
        client.plaza = self.places[0].id;
      }

      var place = self.places.find(function (place) {
        return place.id === client.plaza;
      });
      
      return place;
    }
    
    self.getFormatedPhone = function (phone) {
      //return new libphonenumber.asYouType('US').input(phone.toString())
      return phone;
    }
    
    self.callCenter = function () {
      callPhone(self.getSelectedPlace().phone);
    }
    
    self.login = function () {
      if (self.client && !!self.client.celular_cliente) {
        if (!!self.client.correo_cliente && validateEmail(self.client.correo_cliente)) {
          ons
            .createDialog('loading.html', { parentScope: $scope })
            .then(function(dialog) {
              dialog.show();
            });
          
          var client = angular.copy(self.client);
          //self.readOnlyPhone = true;

          if (!client.plaza) {
            client.plaza = self.places[0].id;
          }
          
          var place = self.places.find(function (place) {
            return place.id === client.plaza;
          });
          
          client.poblacion_cliente = place.name;
          
          Storage.setPlacePhone({ phone: place.phone });
          WS.getClientInfo(client, function (err, data) {
            $scope.loadingModal.hide();
            if (err) return;

            if (!!data) {
              if (+data.Status_Registro === 1) {
                Storage.getFirstLogin(function (result) {
                  if (result) {
                    Storage.setUserData(data);
                    var profile = '';
                    
                    if (data.profile.length === 1) {
                      profile = data.profile[0]
                    }
                    
                    Storage.setProfile({ profile: profile });
                    startNavigator.resetToPage('main.html', { data: { client: data, profile: profile } });
                  } else {
                    startNavigator.pushPage('views/start/registered.html', { data: data });
                  }
                })
              } else if(+data.Status_Registro !== 0) {
                ons
                  .notification
                  .alert({
                    title: 'Registro', 
                    message: 'Tus datos estan siendo procesados. Se le notificara cuando pueda ingresar a la aplicación.' 
                  })
                  .then(function () {
                    
                  });
              }
            } else {
              startNavigator.pushPage('views/start/not-registered.html', { data: client });
            }
          });  
        } else {
          
        }
      }
    }
  });
  
  app.controller('NotRegisteredController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.client = page.data;

        WS.getPlaces({}, function (err, places) {
          if (err) return;

          self.places = places;
        })
      }
    }
    
    self.register = function () {
      var client = angular.copy(self.client);
      
      if (!!client.nombre_cliente && client.correo_cliente && client.direccion_cliente && client.colonia_cliente && client.tipo_servicio) {
        WS.registerInfoClient(client, function (err, client) {
          Storage.setPhoneNumber({ phone: client.celular_cliente });
          Storage.setLastEmail({ email: client.correo_cliente });
          
          ons
            .notification
            .alert({
              title: 'Registro', 
              message: 'Tus datos han sido enviados, sera notificado cuando su información sea dada de alta en el sistema.' 
            })
            .then(function () {
              $scope.startNavigator.resetToPage('views/start/start.html')
            });
        }); 
      }
    }
  });
  
  app.controller('RegisteredController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        WS.getPlaces({}, function (err, places) {
          if (err) return;
          
          self.places = places;
          
          self.client = page.data;
          self.client.direccion_cliente = self.client.nombre_calle_cliente + ' #' + self.client.num_casa_cliente
          self.client.direction_cliente += self.client.num_int_cliente ? ('No. Interior: ' + self.client.num_int_cliente) : '';
        })
      }
    }
    
    self.edit = function () {
      var client = angular.copy(self.client);
      $scope.startNavigator.pushPage('views/start/not-registered.html', { data: client })
    }
    
    self.next = function () {
      var client = angular.copy(self.client);
      Storage.setPhoneNumber({ phone: client.celular_cliente });
      Storage.setLastEmail({ email: client.correo_cliente });
      Storage.setUserData(client);
      Storage.setFirstLogin(true);
      
      if (client.profile.includes(PROFILE_CI) && client.profile.length === 1) {
        Storage.setProfile({ profile: '' })
        $scope.startNavigator.pushPage('views/settings/settings_ci.html', { data: client })
      } else {
        Storage.setProfile({ profile: PROFILE_TE })
        $scope.startNavigator.pushPage('views/settings_te/settings_te.html', { data: client })
      }
    }
  });
  
  app.controller('ChooseController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.client = page.data;
      }
    }
    
    self.selectService = function (profile) {
      var client = angular.copy(self.client);
      Storage.setProfile({ profile: profile })
      
      $scope.startNavigator.resetToPage('main.html', { data: { client: client, profile: profile } });
    }
  });

  app.controller('SplitterController', function($scope, WS) {
    var self = this;
    
    self.backTrace = [];
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        mySplitter.backTracePop = function () {
          var lastPage = self.backTrace.splice(-1,1)[0];
          self.selectedPage = lastPage;
          mySplitter.content.load(lastPage)
            .then(function() {
              mySplitter.left.close();
            });
        }
        
        mySplitter.onDeviceBackButton.setListener(function (e) {
          if (self.backTrace.length > 0) {
            mySplitter.backTracePop();  
          } else {
            e.callParentHandler();
          }
        });
        
        mySplitter.getUserImage = function () {
          return 'url("' + mySplitter.userImageUrl + '")';
        }
        
        mySplitter.load = function(page) {
          if (!page) {
            page = this.defaultView();
          }

          self.backTrace.push(self.selectedPage);
          self.selectedPage = page;
          mySplitter.content.load(page)
            .then(function() {
              mySplitter.left.close();
            });
        };
        
        Storage.getUserImageBase64(function (imageUrl) {
          if (!!imageUrl && imageUrl !== 'null') {
            $scope.$apply(function () {
              mySplitter.userImageUrl = imageUrl;
            }); 
          }
        });
        
        self.profile = page.data.profile;
        self.userData = page.data.client;
        self.celular_cliente = new libphonenumber.asYouType('US').input(self.userData.celular_cliente.toString());
        
        if (!self.profile && self.userData.profile.length > 1) {
          var client = angular.copy(self.userData);
          $scope.startNavigator.pushPage('views/start/choose.html', { data: client })
          return;
        }

        WS.getMenuOptions({ profile: this.profile }, function (err, options) {
          if (err) return;

          self.OPTIONS = options;
        });
        
        self.selectedPage = self.defaultView();
      }
    }
    
    this.loadHome = function () {
      if (self.profile === PROFILE_CI) {
        var view = 'views/home/index.html';
        Storage.getCurrentOrder(function (currentOrder) {
          if (!!currentOrder) {
            switch(currentOrder.status) {
              case ORDER_ASSIGNING: 
                view = 'views/home/index_assiging.html';
                break;
              case ORDER_IN_ROUTE: 
                view = 'views/home/index_in_route.html';
                break;
              case ORDER_ARRIVED: 
                view = 'views/home/index_arrived.html';
                break;
              case ORDER_RATING: 
                view = 'views/home/index_rating.html';
                break;
            }
          }
        }); 
        
        return mySplitter.load(view);
        
      } else {
        return mySplitter.load('views/home_te/index.html'); 
      }
    }
    
    this.defaultView = function () {
      self.profile = Storage.getLocalProfile();
      return defaultView(self.profile, Storage.getCurrentOrderState());
    }
    
    self.selectService = function () {
      Storage.setProfile({ profile: '' })
      Storage.getUserData(function (client) {
        $scope.startNavigator.resetToPage('main.html', { data: { client: client, profile: '' } });
      })
    }
    
    self.logout = function () {
      Storage.setConfig(null);
      Storage.setProfile({ profile: '' });
      Storage.setCurrentOrder(null);
      Storage.setUserData(null);
      
      window.location.reload();
    }
    
    self.showUserProfile = function () {
      mySplitter.load('views/user_profile/index.html');
    }
  });
  
  app.controller('UserProfileController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        Storage.getUserData(function (data) {
          self.client = data;
        });
      }
    }
    
    self.close = function () {
      mySplitter.backTracePop();
    }
    
    self.imagePicker = function () {
      window.imagePicker.getPictures(
        function(result) {
          var url = '';
          if (Array.isArray(result)) {
            url = result[0];
          } else {
            url = result;
          }
          
          if (!!url) {
            toDataURL(url, function(dataUrl) {
              $scope.$apply(function () {
                mySplitter.userImageUrl = dataUrl;
                Storage.setUserImageBase64({ image: dataUrl });
              });
            });
          }
          
        }, function (error) {
            console.log('Error: ' + error);
        },
        {
          maximumImagesCount: 1,
          width: 450,
          height: 450,
        }
      )
    }
  });
  
  app.controller('HomeController', function ($scope, WS) {
    var self = this;
    self.order = { cantidad: 1, aditivo: true }
    
    self.quantities = []
    for(var i = 1; i <= 10; i++) {
      self.quantities.push(i);
    }
    
    self.addQuantity = function (quantity) {
      if (self.order.cantidad + quantity > 0) {
        self.order.cantidad += quantity;
      }
    }
    
    self.init = function(e) {
      // Ensure the emitter is the current page, not a nested one
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        Storage.getCurrentOrder(function (currentOrder) {
          if (!!currentOrder) {
            switch(currentOrder.status) {
              case ORDER_ASSIGNING: 
                homeNavigator.resetToPage('views/home/assign.html', { data: currentOrder });
                break;
              case ORDER_IN_ROUTE: 
                homeNavigator.resetToPage('views/home/in-route.html', { data: currentOrder });
                break;
              case ORDER_ARRIVED: 
                homeNavigator.resetToPage('views/home/in-route.html', { data: currentOrder });
                break;
              case ORDER_RATING: 
                homeNavigator.resetToPage('views/home/rate.html', { data: currentOrder });
                break;
            }
          }
        });
        
        if (!self.cylinders || self.cylinders.length === 0){
          Storage.getUserData(function (currentUser) {
            if (!!currentUser) {
              WS.getCylindersInfo(currentUser, function (err, cylinders) {
                self.cylinders = cylinders.cil_precios.map(function (cil) {
                  return cil.ConsultaPrecio;
                }).sort(function (a, b) {
                  return +a.capacidad_cil - +b.capacidad_cil
                });
                self.cylinderChanged(0)
              }); 
            }
          });
        }
      }
    };
    
    self.cylinderChanged = function (index) {
      self.selectedCylinder = self.cylinders[index];
    }
    
    self.getPrice = function () {
      if (self.selectedCylinder) {
        var base = self.order.aditivo 
          ? +self.selectedCylinder.p_sin_aditivo + +self.selectedCylinder.p_aditivo
          : +self.selectedCylinder.p_sin_aditivo;

        return base * self.order.cantidad;
      }
    }
    
    self.orderCylinder = function () {
      var order = angular.copy(self.order);
      order.capacidad_cilindro = self.selectedCylinder.capacidad_cil
      order.total = self.getPrice();
      homeNavigator.pushPage('views/home/request-info.html', { data: order })
    }
  });
  
  app.controller('HomeControllerTE', function ($scope) {
    this.init = function () {
      
    }
    
    $scope.cancelSchedule = function() {
      $scope.scheduleModal.hide();
    }
    
    $scope.confirmSchedule = function () {
      $scope.scheduleModal.hide();
      ons
        .notification
        .alert({ title: 'Confirmación', message: 'Cita confirmada' })
        .then(function () {
          $scope.$apply(function () {
            $scope.scheduledData = {};
          });
        });
    }
    
    $scope.schedule = function () {
      ons
        .createDialog('views/home_te/dialog_schedule.html', { parentScope: $scope })
        .then(function(dialog) {
          dialog.show();
        });
    }
  });
  
  app.controller('OrderController', function ($scope) {
    $scope.cancelSchedule = function() {
      $scope.scheduleModal.hide();
    }
    
    $scope.confirmSchedule = function () {
      $scope.scheduleModal.hide();
      ons
        .notification
        .alert({ title: 'Confirmación', message: 'Cita confirmada' })
        .then(function () {
          $scope.$apply(function () {
            $scope.scheduledData = {};
          });
        });
    }
    
    $scope.schedule = function () {
      ons
        .createDialog('views/home_te/dialog_schedule.html', { parentScope: $scope })
        .then(function(dialog) {
          dialog.show();
        });
    }
  });
  
  app.controller('RequestController', function ($scope, WS) {
    var self = this;
    
    self.init = function(e) {
      // Ensure the emitter is the current page, not a nested one
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.order = page.data;
        
        Storage.getUserData(function (user) {
          self.client = user;
          WS.getAddresses(user, function (err, addresses) {
            self.addresses = addresses.filter(function (place) {
              return place.tipo_servicio === 'P'
            });

            self.selectedAddress = self.addresses.length > 1 ? null : self.addresses[0];
            
            if (self.addresses.length > 1) {
              self.order.padron_id = -1;
            }
          })
        })
      }
    };
    
    self.addressChanged = function () {
      self.selectedAddress = self.addresses.find(function (address) {
        return address.padron_id === self.order.padron_id;
      });
    };
    
    $scope.confirmRequest = function () {
      var order = angular.copy(self.order);
      
      if (order.padron_id === -1) {
        return ons
          .notification
          .alert({
            title: 'Dirección requerida', 
            message: 'Debe seleccionar una dirección.' 
          })
          .then(function () {

          });
      }
      
      if (!order.padron_id) {
        order.padron_id = self.addresses[0].padron_id;
      }
      
      order.direccion = self.addresses.find(function (address) {
        return address.padron_id === order.padron_id;
      });
      
      order.aditivo = order.aditivo ? 'S' : 'N';
      order.plaza = self.client.plaza;
      order.celular_cliente = self.client.celular_cliente;
      
      WS.orderCylinder(order, function (err, order) {
        if (err) return;
        
        order.status = ORDER_ASSIGNING;
        Storage.setCurrentOrder(order);
        homeNavigator.resetToPage('views/home/assign.html', { data: order });
      });
    }
  })
  
  app.controller('AssignController', function($scope, WS, $timeout, $interval) {
    var self = this;
    var interval;
    
    self.onInit = function () {
      var successAssign = function (order) {
          order.status = ORDER_IN_ROUTE;

          Storage.setCurrentOrder(order);
          $scope.homeNavigator.replacePage('views/home/in-route.html', { data: order });
        };
        
        self.order.tipo_servicio = 'P';
        WS.assignUnitOrder(self.order, function (err, order) {
            if (!order.unit || !order.unit.nombre_operador) {
              interval = $interval(function () {
                WS.assignUnitOrder(self.order, function (err, order) {
                  if (!!order.unit && !!order.unit.nombre_operador) {
                    $interval.cancel(interval);
                    successAssign(order);
                  }
                })
              }, REQUEST_CHECK_INTERVAL_SHORT);
            } else {
              successAssign(order);
            }
        });
    }
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.order = page.data;
        
        if (!self.order.cantidad) {
          Storage.getCurrentOrder(function (order) {
            self.order = order;
            self.onInit();
          });
        } else {
          self.onInit();
        }
      }
    }
    
    self.destroy = function (e) {
      $interval.cancel(interval);
    }
    
    self.cancel = function () {
      ons.notification.confirm({title: 'Cancelar pedido', message: '¿Esta seguro de cancelar el pedido?'}).then(function (result) {
        if (!!result) {
          $interval.cancel(interval);
          
          WS.cancelOrder(self.order, function (err, data) {
            Storage.setCurrentOrder(null);
            homeNavigator.replacePage('views/home/home.html');
          });
        }
      });
    }
  })
  
  app.controller('InRouteController', function ($scope, WS, $timeout, $interval) {
    
    var self = this;
    var interval;
    var timer;
    
    
    self.onInit = function () {
      var orderArrived = function (order) {
          order.status = ORDER_ARRIVED;
          self.order = order;
          
          Storage.setCurrentOrder(order);
        };
      
      var cronometer = document.querySelector('#cronometer');
      $timeout(function () {
        cronometer.style.width = cronometer.offsetHeight + 'px';
        document.querySelector('#text-cronometer').style.fontSize = cronometer.offsetHeight / 7 + 'px';
        document.querySelector('#text-cronometer-sm').style.fontSize = cronometer.offsetHeight / 15 + 'px';
       }, 50);

        if (self.order.status === ORDER_IN_ROUTE) {
          // Decremental
          /*
          var promise = moment(self.order.hora_registro).add(MINUTES_PROMISE, 'minutes')//.add(1, 'hour');
          var seconds = promise.diff(moment(), 'seconds');
          if (seconds < 0) {
            seconds = 0;
          }
          
          self.progress = (MINUTES_PROMISE * 60 - seconds) / (MINUTES_PROMISE * 60) * 100 + '%';
          self.time = moment.unix(seconds);
          
          timer = $interval(function () {
            if (self.time.isAfter(moment.unix(0))) {
              self.time.subtract(1, 'second');
              
              var seconds = self.time.minute() * 60 + self.time.second();
              self.progress = (MINUTES_PROMISE * 60 - seconds) / (MINUTES_PROMISE * 60) * 100 + '%';
            } else {
              $interval.cancel(timer);
            }
          }, 1000);
          */
          
          // Incremental
          var promise = moment(self.order.hora_registro).add(MINUTES_PROMISE, 'minutes')//.add(1, 'hour');
          var register = moment(self.order.hora_registro)//.add(1, 'hour');
          var now = moment();
          
          var seconds = now.diff(register, 'seconds');
          if (seconds < 0) {
            seconds = 0;
          }
          
          moveSpinner((seconds / 60) * 6);
          
          self.progress = (seconds) / (MINUTES_PROMISE * 60) * 100 + '%';
          self.time = moment.unix(seconds);
          
          timer = $interval(function () {
            if (self.time.isAfter(moment.unix(0))) {
              self.time.add(1, 'second');
              
              var seconds = self.time.minute() * 60 + self.time.second();
              self.progress = (seconds) / (MINUTES_PROMISE * 60) * 100 + '%';
              moveSpinner((seconds / 60) * 6);
              
            } else {
              $interval.cancel(timer);
            }
          }, 1000);
          
          function  moveSpinner(degrees) {
            degrees = degrees == 360 ? 0 : degrees;
            degrees += (degrees > 360) ? -(parseInt(degrees / 360) * 360) : 0;
            
            var spinner = document.querySelector('.spinner');
            var filler = document.querySelector('.filler');
            var mask = document.querySelector('.mask');
            var clock = document.querySelector('.clock-layout');
            var img = document.querySelector('#imgCronometer');
            
            img.style.transform = 'rotate('+ (degrees - 90) +'deg) translateX('+ (84 / 250 * cronometer.offsetHeight) +'px) rotate('+ (degrees - 90) * -1 +'deg)';

            spinner.style.transform = 'rotate(' + degrees + 'deg)';
            if (degrees > 180) {
              filler.style.opacity = '1';
              mask.style.opacity = '0';
            } else {
              filler.style.opacity = '0';
              mask.style.opacity = '1';
            }
          }

          function round(deg, interval) {
            return Math.ceil(deg / interval) * interval;
          }

          interval = $interval(function () {
            WS.checkArriveOrder(self.order, function (err, order) {
              if (!!order.hora_llegada && moment(order.hora_llegada).isValid()) {
                $interval.cancel(interval);
                $interval.cancel(timer);
                orderArrived(order); 
              }
            })
          }, REQUEST_CHECK_INTERVAL_LONG);
        }
    }
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.order = page.data;
        if (!self.order.cantidad) {
          
          Storage.getCurrentOrder(function (order) {
            self.order = order;
            self.onInit();
          });
        } else {
          self.onInit();
        }
      }
    }
    
    self.destroy = function (e) {
      $interval.cancel(interval);
      $interval.cancel(timer);
    }
    
    self.cancel = function () {
      ons.notification.confirm({title: 'Cancelar pedido', message: '¿Esta seguro de cancelar el pedido?'}).then(function (result) {
        if (!!result) {
          $interval.cancel(interval);
          $interval.cancel(timer);

          WS.cancelOrder(self.order, function (err, data) {
            Storage.setCurrentOrder(null);
            homeNavigator.replacePage('views/home/home.html');
          });
        }
      });
    }
    
    self.confirmArrive = function () {
      var order = angular.copy(self.order)
      order.status = ORDER_RATING;
      
      Storage.setCurrentOrder(order);
      homeNavigator.replacePage('views/home/rate.html', { data: order })
    }
  })
  
  app.controller('RateController', function ($scope, WS) {
    $scope.maxStars = 5;
    $scope.stars = [];
    
    var self = this;
    
    self.onInit = function () {
      self.order.rating = {};
        
      WS.getOrderRateQuestions(self.order, function (err, data) {
        self.questions = data;
      });
    }
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.order = page.data;
        
        if (!self.order.cantidad) {
          Storage.getCurrentOrder(function (order) {
            self.order = order;
            self.onInit();
          });
        } else {
          self.onInit();
        }
      }
    }
    
    for(var i = 0; i < $scope.maxStars; i++) {
      $scope.stars.push(i + 1); 
    }
    
    $scope.setRating = function (star) {
      self.order.rating.calificacion = star;
    }
    
    $scope.sendRating = function () {
      var orderRating = {
        plaza: self.order.plaza,
        celular_cliente: self.order.celular_cliente,
        nombre_operador: self.order.unit.nombre_operador || '',
        calificacion: self.order.rating.calificacion || 0,
        descripcion_problema: self.order.rating.descripcion_problema || 'Sin observaciones',
        resp_pregunta_1: self.order.rating.pregunta_1 || false,
        resp_pregunta_2: self.order.rating.pregunta_2 || false
      };
      
      WS.rateOrder(orderRating, function(err, order) {
        $scope.skip();
      });
    }
    
    $scope.skip = function () {
      Storage.setCurrentOrder(null);
      $scope.homeNavigator.replacePage('views/home/home.html');
    }
  });
  
  app.controller('BillingController', function ($scope) {
    $scope.edit = function () {
      $scope.billingNavigator.pushPage('views/billing/register.html');
    }
  });
  
  app.controller('RegisterBillingController', function ($scope) {
    $scope.save = function () {
      $scope.billingNavigator.popPage();
    }
  });
  
  app.controller('SettingsCIController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.client = page.data;
        self.config = {}
        
        if (!self.client.celular_cliente) {
          Storage.getUserData(function (client) {
            self.client = client;
          });
        }
        
        Storage.getConfig(function (config) {
          self.config = config;
        })
      }
    }
    
    self.next = function () {
      var config = { status_facturas: false, status_promociones: false };
      self.config = self.config || {};
      
      config.status_facturas = self.config.status_facturas || false;
      config.status_promociones = self.config.status_promociones || false;
      config.celular_cliente = self.client.celular_cliente;
      config.plaza = self.client.plaza;
      
      WS.saveGrlConfig(config, function (err, config) {
        var client = angular.copy(self.client);
        Storage.setConfig(config);

        if (client.profile.includes(PROFILE_TE)) {
          Storage.setProfile({ profile: '' })
          $scope.startNavigator.pushPage('views/settings_te/settings_te.html', { data: client });
        } else {
          Storage.setProfile({ profile: PROFILE_CI })
          $scope.startNavigator.resetToPage('main.html', { data: { client: client, profile: PROFILE_CI } });
        }
      });
    }
    
    self.save = function () {
      var config = { status_facturas: false, status_promociones: false };
      self.config = self.config || {};
      
      config.status_facturas = self.config.status_facturas || false;
      config.status_promociones = self.config.status_promociones || false;
      config.celular_cliente = self.client.celular_cliente;
      config.plaza = self.client.plaza;
      
      WS.saveGrlConfig(config, function (err, config) {
        var client = angular.copy(self.client);
        Storage.setConfig(config);
        mySplitter.load(defaultView(Storage.getLocalProfile(), Storage.getCurrentOrderState()));
      });
    }
  });
  
  app.controller('SettingsTEController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.client = page.data;
        self.config = {}
        
        if (!self.client.celular_cliente) {
          Storage.getUserData(function (client) {
            self.client = client;
          });
        }
        
        self.config.time = {
          hour: 6,
          minutes: 30,
          period: 'AM',
          dayBefore: true
        }
        
        Storage.getConfig(function (config) {
          if (!config.time) {
            config.time = self.config.time;
          }
          self.config = config;
        })
      }
    }
    
    self.next = function () {
      var config = angular.copy(self.config);
      
      config.status_facturas = self.config.status_facturas || false;
      config.status_promociones = self.config.status_promociones || false;
      config.celular_cliente = self.client.celular_cliente;
      config.plaza = self.client.plaza;
      
      WS.saveGrlConfig(config, function (err, config) {
        var client = angular.copy(self.client);
        var profile = '';
        if (client.profile.length === 1) {
          profile = PROFILE_TE;
        }
        
        Storage.setConfig(config);
        Storage.setProfile({ profile: profile });
        $scope.startNavigator.resetToPage('main.html', { data: { client: client, profile: profile } });
      });
    }
    
    self.save = function () {
      var config = angular.copy(self.config);
      
      config.status_facturas = self.config.status_facturas || false;
      config.status_promociones = self.config.status_promociones || false;
      config.celular_cliente = self.client.celular_cliente;
      config.plaza = self.client.plaza;
      
      WS.saveGrlConfig(config, function (err, config) {
        var client = angular.copy(self.client);
        
        Storage.setConfig(config);
        mySplitter.load(defaultView(Storage.getLocalProfile(), Storage.getCurrentOrderState()));
      });
    }
    
    self.cancel = function() {
      $scope.pickerModal.hide();
    }
    
    self.confirm = function () {
      self.config.time = angular.copy(self.time);
      $scope.pickerModal.hide();
    }
    
    self.showPicker = function () {
      ons
        .createDialog('views/start/picker.html', { parentScope: $scope })
        .then(function(dialog) {
          self.time = angular.copy(self.config.time);
          self.selectHour();
          dialog.show();
        });
    }
    
    self.selectHour = function () {
      self.hourSelected = true;
      self.spinnerText = 'horas';
      self.spinnerValue = self.time.hour;
      self.spinnerBase = 30;
    }
    
    self.selectMinutes = function () {
      self.hourSelected = false; 
      self.spinnerText = 'minutos';
      self.spinnerValue = self.time.minutes;
      self.spinnerBase = 6;
    }
    
    self.selectPeriod = function (period) {
      self.time.period = period;
    }
    
    $scope.change = function (value) {
      if (self.hourSelected) {
        self.time.hour = value;
      } else {
        self.time.minutes = value;
      }
    }
  });
  
  app.controller('PromotionsController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        self.selectedPromotion = 0;
        Storage.getUserData(function (client) {
          WS.getPromotions(client, function (err, promotions) {
            self.promotions = promotions;
          })
        });
      }
    }
  });
  
  app.controller('PointsController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        Storage.getUserData(function (client) {
          self.client = client;
          WS.getAddresses(client, function (err, addresses) {
            client.padron_id = addresses[0].padron_id;
            WS.getConsumptionInfo(client, function (consumption) {
              self.consumption = consumption;
            });
          })
        });
      }
    }
    
    self.loadOrderCil = function () {
      mySplitter.load(defaultView(Storage.getLocalProfile(), Storage.getCurrentOrderState()));
    }
    
    self.loadConsumption = function () {
      mySplitter.load('views/consumption/index.html');
    }
  });
  
  app.controller('ConsumptionController', function ($scope, WS) {
    var self = this;
    
    self.init = function (e) {
      if (e.target === e.currentTarget) {
        var page = e.target;
        
        Storage.getUserData(function (client) {
          self.client = client;
          WS.getAddresses(client, function (err, addresses) {
            client.padron_id = addresses[0].padron_id;
            WS.getConsumptionInfo(client, function (err, consumption) {
              self.consumption = consumption;
            });
          })
        });
      }
    }
    
    self.loadOrderCil = function () {
      mySplitter.load(defaultView(Storage.getLocalProfile(), Storage.getCurrentOrderState()));
    }
    
    self.loadConsumption = function () {
      mySplitter.load('views/consumption/index.html');
    }
  });
  
   app.controller('TankController', function ($scope) {
        
         
     var myCanvas = document.getElementById("myCanvas");
myCanvas.width = 200;
myCanvas.height = 300;
       
  
var ctx = myCanvas.getContext("2d");
         
         var myCanvas2 = document.getElementById("myCanvas2");
myCanvas2.width = 200;
myCanvas2.height = 300;
var num1=0;
  
var ctx = myCanvas2.getContext("2d");
         
         var myCanvas3 = document.getElementById("myCanvas3");
myCanvas3.width = 200;
myCanvas3.height = 300;
var num1=0;
  
var ctx = myCanvas3.getContext("2d");

function drawLine(ctx, startX, startY, endX, endY,color){
    ctx.save();
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(endX,endY);
    ctx.stroke();
    ctx.restore();
}
function drawBar(ctx, upperLeftCornerX, upperLeftCornerY, width, height,color){
    ctx.save();
    ctx.fillStyle=color;
    ctx.fillRect(upperLeftCornerX,upperLeftCornerY,width,height);
    ctx.restore();
}

var myVinyls = {
    "1": 80
};
         
         var myVinyls2 = {
    "1": 70
};
var Barchart = function(options){
    this.options = options;
    this.canvas = options.canvas;
    this.ctx = this.canvas.getContext("2d");
    this.colors = options.colors;
  
    this.draw = function(){
        var maxValue = 100;
        for (var categ in this.options.data){
            maxValue = Math.max(maxValue,this.options.data[categ]);
        }
        var canvasActualHeight = this.canvas.height - this.options.padding * 2;
        var canvasActualWidth = this.canvas.width - this.options.padding * 2;
 
        //drawing the grid lines
        var gridValue = 0;
        while (gridValue <= maxValue){
            var gridY = canvasActualHeight * (1 - gridValue/maxValue) + this.options.padding;
            drawLine(
                this.ctx,
                0,
                gridY,
                this.canvas.width,
                gridY,
                this.options.gridColor
            );
             
            //writing grid markers
            this.ctx.save();
            this.ctx.fillStyle = this.options.gridColor;
            this.ctx.font = "bold 10px Arial";
            this.ctx.fillText(gridValue, 1,gridY - 2);
            this.ctx.restore();
 
            gridValue+=this.options.gridScale;
        }
  
        //drawing the bars
        var barIndex = 0;
        var numberOfBars = Object.keys(this.options.data).length;
        var barSize = (canvasActualWidth)/numberOfBars;
 
        for (categ in this.options.data){
            var val = this.options.data[categ];
            var barHeight = Math.round( canvasActualHeight * val/maxValue) ;
            drawBar(
                this.ctx,
                this.options.padding + barIndex * barSize,
                this.canvas.height - barHeight - this.options.padding,
                barSize,
                barHeight,
                this.colors[barIndex%this.colors.length]
            );
 
            barIndex++;
        }

        this.ctx.save();
this.ctx.textBaseline="bottom";
this.ctx.textAlign="center";
this.ctx.fillStyle = "#000000";
this.ctx.font = "bold 60px Arial";
this.ctx.fillText(this.options.seriesName, this.canvas.width/2,this.canvas.height);
this.ctx.restore();  
  
    }
}
var myBarchart = new Barchart(
    {
        canvas:myCanvas,
        seriesName:"%" + myVinyls["1"],
        padding:10,
        gridScale:5,
        gridColor:"#eeeeee",
        data:myVinyls,
        colors:["#9F9C9C"]
    }
);
         
var myBarchart2 = new Barchart(
    {
        canvas:myCanvas2,
        seriesName:"%" + myVinyls2["1"],
        padding:10,
        gridScale:5,
        gridColor:"#eeeeee",
        data:myVinyls,
        colors:["#9F9C9C"]
    }
);
         var myBarchart3 = new Barchart(
    {
        canvas:myCanvas3,
        seriesName:"%" + myVinyls["1"],
        padding:10,
        gridScale:5,
        gridColor:"#eeeeee",
        data:myVinyls,
        colors:["#9F9C9C"]
    }
);


        myBarchart.draw();
         myBarchart2.draw();
         myBarchart3.draw();
         
         
         
         
    $scope.open = function () {
      $scope.tanksNavigator.pushPage('views/tanks/tank.html');
    }
  });
    app.controller('TankInformationController', function ($scope) {
             new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: ["Enero", "Fevrero", "Marzo", "Avril", "Mayo","Junio","Julio","Agosto","Septiemvre","Octuvre","Noviemvre","Diciemvre",],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3cba9f","#e8c3b9","#c45850",,"#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433,2478,5267,734,784,433,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Predicted world population (millions) in 2050'
      }
    }
})
             
             new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, { 
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, { 
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});
        
        
    });
})()


