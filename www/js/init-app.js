/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested initialization place for your code.
// It is completely optional and not required.
// It implements a Cordova "hide splashscreen" function, that may be useful.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false, app:false, dev:false */
/*global myEventHandler:false, cordova:false, device:false */



window.app = window.app || {} ;         // there should only be one of these...



// Set to "true" if you want the console.log messages to appear.

app.LOG = app.LOG || false ;

app.consoleLog = function() {           // only emits console.log messages if app.LOG != false
    if( app.LOG ) {
        var args = Array.prototype.slice.call(arguments, 0) ;
        console.log.apply(console, args) ;
    }
} ;



// App init point (runs on custom app.Ready event from init-dev.js).
// Runs after underlying device native code and webview/browser is ready.
// Where you should "kick off" your application by initializing app events, etc.

// NOTE: Customize this function to initialize your application, as needed.

app.initEvents = function() {
    "use strict" ;
    var fName = "app.initEvents():" ;
    app.consoleLog(fName, "entry") ;
    // NOTE: initialize your third-party libraries and event handlers

    // initThirdPartyLibraryNumberOne() ;
    // initThirdPartyLibraryNumberTwo() ;
    // initThirdPartyLibraryNumberEtc() ;

    // NOTE: initialize your application code

    // initMyAppCodeNumberOne() ;
    // initMyAppCodeNumberTwo() ;
    // initMyAppCodeNumberEtc() ;

    // NOTE: initialize your app event handlers, see app.js for a simple event handler example

    // TODO: configure following to work with both touch and click events (mouse + touch)
    // see http://msopentech.com/blog/2013/09/16/add-pinch-pointer-events-apache-cordova-phonegap-app/

    var el, evt ;

    if( navigator.msPointerEnabled || !('ontouchend' in window))    // if on Win 8 machine or no touch
        evt = "click" ;                                             // let touch become a click event
    else                                                            // else, assume touch events available
        evt = "touchend" ;                                          // not optimum, but works

    // NOTE: ...you can put other miscellaneous init stuff in this function...
    // NOTE: ...and add whatever else you want to do now that the app has started...
    // NOTE: ...or create your own init handlers outside of this file that trigger off the "app.Ready" event...

    app.initDebug() ;           // just for debug, not required; keep it if you want it or get rid of it
    app.hideSplashScreen() ;    // after init is good time to remove splash screen; using a splash screen is optional

    // app initialization is done
    // app event handlers are ready
    // exit to idle state and wait for app events...

    app.consoleLog(fName, "exit") ;
} ;
document.addEventListener("app.Ready", app.initEvents, false) ;



// Just a bunch of useful debug console.log() messages.
// Runs after underlying device native code and webview/browser is ready.
// The following is just for debug, not required; keep it if you want or get rid of it.

app.initDebug = function() {
    "use strict" ;
    var fName = "app.initDebug():" ;
    app.consoleLog(fName, "entry") ;

    if( window.device && device.cordova ) {                     // old Cordova 2.x version detection
        app.consoleLog("device.version: " + device.cordova) ;   // print the cordova version string...
        app.consoleLog("device.model: " + device.model) ;
        app.consoleLog("device.platform: " + device.platform) ;
        app.consoleLog("device.version: " + device.version) ;
    }

    if( window.cordova && cordova.version ) {                   // only works in Cordova 3.x
        app.consoleLog("cordova.version: " + cordova.version) ; // print new Cordova 3.x version string...

        if( cordova.require ) {                                 // print included cordova plugins
            app.consoleLog(JSON.stringify(cordova.require('cordova/plugin_list').metadata, null, 1)) ;
        }
    }

    app.consoleLog(fName, "exit") ;
} ;



// Using a splash screen is optional. This function will not fail if none is present.
// This is also a simple study in the art of multi-platform device API detection.

app.hideSplashScreen = function() {
    "use strict" ;
    var fName = "app.hideSplashScreen():" ;
    app.consoleLog(fName, "entry") ;

    // see https://github.com/01org/appframework/blob/master/documentation/detail/%24.ui.launch.md
    // Do the following if you disabled App Framework autolaunch (in index.html, for example)
    // $.ui.launch() ;

    if( navigator.splashscreen && navigator.splashscreen.hide ) {   // Cordova API detected
        navigator.splashscreen.hide() ;
    }
    if( window.intel && intel.xdk && intel.xdk.device ) {           // Intel XDK device API detected, but...
        if( intel.xdk.device.hideSplashScreen )                     // ...hideSplashScreen() is inside the base plugin
            intel.xdk.device.hideSplashScreen() ;
    }

    app.consoleLog(fName, "exit") ;
} ;

function hasReadPermission(successCallback, errorCallback) {
  window.plugins.sim.hasReadPermission(successCallback, errorCallback);
}
 
// request permission 
function requestReadPermission(successCallback, errorCallback) {
  window.plugins.sim.requestReadPermission(successCallback, errorCallback);
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function toDataURL(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    var reader = new FileReader();
    reader.onloadend = function() {
      callback(reader.result);
    }
    reader.readAsDataURL(xhr.response);
  };
  
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
}

var CALL_CENTER_NUMBER = '';
var REPORT_NUMBER = '';

var MINUTES_PROMISE = 30;
var REQUEST_CHECK_INTERVAL_SHORT = 1000 * 30; // 30 seconds
var REQUEST_CHECK_INTERVAL_LONG = 1000 * 60 * 2; // 2 minutes

var USER_PROFILE = 'user_profile_value';
var PROFILE_CI = 'CI';
var PROFILE_TE = 'TE';
var USER_DATA = 'USER_DATA';
var CUR_ORDER = 'CUR_ORDER';
var USER_CONFIG = 'USER_CONFIG';

var ORDER_ASSIGNING = 'ORDER_ASSIGNING';
var ORDER_IN_ROUTE = 'ORDER_IN_ROUTE';
var ORDER_ARRIVED = 'ORDER_ARRIVED';
var ORDER_RATING = 'ORDER_RATING';

var CUR_ORDER_STATE = 'CUR_ORDER_STATE';

var PHONE_NUMBER = 'PHONE_NUMBER';
var LAST_EMAIL = 'LAST_EMAIL';
var USER_IMAGE64 = 'USER_IMAGE64';
var PLACE_PHONE = 'PLACE_PHONE';

var FIRST_LOGIN = 'FIRST_LOGIN';

var real = true;

var Storage = {
  setFirstLogin: function (data) {
    if (real) {
      NativeStorage.putString(FIRST_LOGIN, JSON.stringify(data)); 
    } else {
      localStorage.setItem(FIRST_LOGIN, JSON.stringify(data)); 
    }
  },
  getFirstLogin: function (callback) {
    var success = function (data) {
      callback(JSON.parse(data));
    }
    
    if (real) {
      NativeStorage.getString(FIRST_LOGIN, success); 
    } else {
      success(localStorage.getItem(FIRST_LOGIN));
    }
  },
  setLastEmail: function (data) {
    if (real) {
      NativeStorage.putString(LAST_EMAIL, data.email); 
    } else {
      localStorage.setItem(LAST_EMAIL, data.email); 
    }
  },
  getLastEmail: function (callback) {
    var success = function (phone) {
      callback(phone);
    }
    
    if (real) {
      NativeStorage.getString(LAST_EMAIL, success); 
    } else {
      success(localStorage.getItem(LAST_EMAIL));
    }
  },
  setPhoneNumber: function (data) {
    if (real) {
      NativeStorage.putString(PHONE_NUMBER, data.phone); 
    } else {
      localStorage.setItem(PHONE_NUMBER, data.phone); 
    }
  },
  getPhoneNumber: function (callback) {
    var success = function (phone) {
      callback(phone);
    }
    
    if (real) {
      NativeStorage.getString(PHONE_NUMBER, success); 
    } else {
      success(localStorage.getItem(PHONE_NUMBER));
    }
  },
  setProfile: function (data) {
    if (real) {
      NativeStorage.putString(USER_PROFILE, data.profile); 
    }
    localStorage.setItem(USER_PROFILE, data.profile);
  },
  getProfile: function (callback) {
    var success = function (profile) {
      callback(profile);
    }
    
    if (real) {
      NativeStorage.getString(USER_PROFILE, success);  
    } else {
      success(localStorage.getItem(USER_PROFILE)); 
    }
  },
  getLocalProfile: function () {
    return localStorage.getItem(USER_PROFILE);
  },
  setCurrentOrderState: function (data) {
    localStorage.setItem(CUR_ORDER_STATE, data.status);
  },
  getCurrentOrderState: function () {
    return localStorage.getItem(CUR_ORDER_STATE);
  },
  setUserData: function (data) {
    if (real) {
      NativeStorage.putString(USER_DATA, JSON.stringify(data));  
    } else {
      localStorage.setItem(USER_DATA, JSON.stringify(data)); 
    }
  },
  getUserData: function (callback) {
    var success = function (data) {
      var userData = null
      if (!!data) {
        userData = JSON.parse(data);
      }
      
      callback(userData);
    }
    
    if (real) {
      NativeStorage.getString(USER_DATA, success);  
    } else {
      success(localStorage.getItem(USER_DATA)); 
    }
  },
  setCurrentOrder: function (data) {
    if (real) {
      NativeStorage.putString(CUR_ORDER, JSON.stringify(data));  
    } else {
      localStorage.setItem(CUR_ORDER, JSON.stringify(data)); 
    }
  },
  getCurrentOrder: function (callback) {
    var success = function (data) {
      var orderData = null;  
      if (!!data) {
        orderData = JSON.parse(data);
      }
      
      callback(orderData);
    }
    
    if (real) {
      NativeStorage.getString(CUR_ORDER, success);  
    } else {
      success(localStorage.getItem(CUR_ORDER)); 
    }
  },
  setConfig: function (data) {
    if (real) {
      NativeStorage.putString(USER_CONFIG, JSON.stringify(data));  
    } else {
      localStorage.setItem(USER_CONFIG, JSON.stringify(data)); 
    }
  },
  getConfig: function (callback) {
    var success = function (data) {
      var configData = null;  
      if (!!data) {
        configData = JSON.parse(data);
      }
      
      callback(configData);
    }
    
    if (real) {
      NativeStorage.getString(USER_CONFIG, success);  
    } else {
      success(localStorage.getItem(USER_CONFIG)); 
    }
  },
  setUserImageBase64: function (data) {
    if (real) {
      NativeStorage.putString(USER_IMAGE64, data.image);   
    } else {
      localStorage.setItem(USER_IMAGE64, data.image); 
    }
  },
  getUserImageBase64: function (callback) {
    var success = function (image) {
      callback(image);
    }
  
    if (real) {
      NativeStorage.getString(USER_IMAGE64, success);  
    } else {
      success(localStorage.getItem(USER_IMAGE64));
    }
  },
  setPlacePhone: function (data) {
    if (real) {
      NativeStorage.putString(PLACE_PHONE, data.phone); 
    }
    localStorage.setItem(PLACE_PHONE, data.phone);
  },
  getPlacePhone: function (callback) {
    var success = function (phone) {
      callback(phone);
    }
    
    if (real) {
      NativeStorage.getString(PLACE_PHONE, success);  
    } else {
      success(localStorage.getItem(PLACE_PHONE)); 
    }
  }
}

function callPhone(number) {
  window.plugins.CallNumber.callNumber(function(result) {
    
  }, function (result) {
    
  }, number, false);
}